========================================
Constrained Consensus Sequence Algorithm
========================================

HELP
----

python ccsa.py -help

======================================================================
Constrained Consensus Sequence Algorithm       version 1.0 --  04/2020
D. Lavenier - CNRS/IRISA
======================================================================

USAGE: python ccsa.py [arguments]
       arguments:
         -read    <filename>    [in]   read file (fastq format)
         -primer  <filename>    [in]   surrounding sequences (fasta format)
         -length  <integer>     [in]   length of the expected consensus sequence
         -out     <filename>    [out]  consensus sequences output (fasta format)
         -graph   <filename>    [out]  kmer overlap graph (gexf format) [optional]




TEST EXAMPLE
------------

python3 ccsa.py -read seq60.fastq -primer primer.fasta -length 900 -out consensus.fasta

======================================================================
Constrained Consensus Sequence Algorithm       version 1.0 --  04/2020
D. Lavenier - CNRS/IRISA
======================================================================

read file name:      seq60.fastq
surrounding seq:     primer.fasta
consensus length:    900
output filename:     consensus.fasta

Get surrounding sequences
  - LEFT:  CCAACAACAAGGCACTCATTAACTAAGGTGGAAGCAACTGTT
  - RIGHT: ACAACCAGTCGCTGGACAATACAGAAGTAGAAACAACTCAAC

Select reads > 900 bp
  - select 60 reads over 60

Select reads based on surrounding sequences
  - get 59 reads

Build Kmer Overlap Graph (KOG)
    len(kmer)    Start                      End
           20    CTAAGGTGGAAGCAACTGTT_20    ACAACCAGTCGCTGGACAAT_909

Search paths in KOG
  - found 27 paths of length: 904 903 902 901 900 899 898 897 896 895 894 893 892 891 890 889 888 887 886 885 884 883 882 881 880 879 878 





